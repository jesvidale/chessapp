import { combineReducers } from 'redux';

import { fen } from './fen';

export default combineReducers({
  fen
});
