import {
  SET_FEN,
} from '../constants/ActionTypes'

const initialState = {
  fen: '8/2p5/8/8/8/8/8/8 w KQkq - 0 1'
}

export const fen = (state = initialState, action) => {
  switch (action.type) {
    case SET_FEN:
      return {
        ...state,
        fen: action.FEN
      }
    default:
      return { ...state }
  }
}
