String.prototype.isLowerCase = function() {
  return this.valueOf().toLowerCase() === this.valueOf();
};

export const getTentative = (coords, pieceType, FENmatrix) => {
  let tentative = []
  switch (pieceType) {
    case 'P':
      if (coords.rowIndex !== 0) {
        if (FENmatrix[coords.rowIndex - 1][coords.colIndex] === 0) {
          tentative.push([coords.rowIndex - 1, coords.colIndex])
        }
        // Dople move
        if (coords.rowIndex === 6) {
          if (FENmatrix[coords.rowIndex - 2][coords.colIndex] === 0) {
            tentative.push([coords.rowIndex - 2, coords.colIndex])
          }
        }
        // Kill right
        if (coords.colIndex !== 7) {
          const diagonalRight = FENmatrix[coords.rowIndex - 1][coords.colIndex + 1]
          if (isNaN(diagonalRight)) {
            if (diagonalRight.isLowerCase()) {
              tentative.push([coords.rowIndex - 1, coords.colIndex + 1])
            }
          }
        }
        // Kill left
        if (coords.colIndex !== 0) {
          const diagonalLeft = FENmatrix[coords.rowIndex - 1][coords.colIndex - 1]
          if (isNaN(diagonalLeft)) {
            if (diagonalLeft.isLowerCase()) {
              tentative.push([coords.rowIndex - 1, coords.colIndex - 1])
            }
          }
        }
      }
      return tentative
    case 'p':
      console.log(coords, pieceType, FENmatrix)
      break;
    default:
      break;
  }
}
