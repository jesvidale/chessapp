export const toFEN = matrix => {
  let FEN = ''
  matrix.forEach((row, indexRow) => {
    let spaces = 0
    row.forEach((col, indexCol) => {
      if (isNaN(col)) {
        FEN = `${FEN}${col}`
      } else {
        spaces+=1
        if (indexCol === 7) {
          FEN = `${FEN}${spaces.toString()}`
        } else if (isNaN(row[indexCol + 1])) {
          FEN = `${FEN}${spaces.toString()}`
          spaces = 0
        }
      }
    })
    if (indexRow !== 7) {
      FEN = `${FEN}/`
    }
  })
  return FEN
}

export const FENtoMatrix = FEN => {
  const piecePlacement = FEN.split(' ')[0].split('/')
  let matrix = []
  piecePlacement.forEach(row => {
    let rowResult = []
    row.split('').forEach(col => {
      if (isNaN(parseInt(col))) {
        rowResult.push(col)
      } else {
        rowResult = [ ...rowResult, ...new Array(parseInt(col)).fill(0)]
      }
    })
    matrix.push(rowResult)
  })
  return matrix
}
