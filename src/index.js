import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from "react-router-dom";
import reportWebVitals from './reportWebVitals';

// Redux dependencies
import { Provider } from 'react-redux';
import store from './store';

// Main stylesheet
import './index.scss';

// Import main components
import Home from './templates/views/Home';

ReactDOM.render(
  <main id="main">
    <Provider store={ store }>
      <Router>
        <Route path="/" exact component={ Home }/>
      </Router>
    </Provider>
  </main>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
