import * as types from '../constants/ActionTypes';

export const setFen = FEN => ({
  type: types.SET_FEN,
  FEN
})
