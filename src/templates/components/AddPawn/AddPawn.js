import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { setFen } from '../../../actions';

import {FENtoMatrix, toFEN} from '../../../scripts/FEN-parser.js';

import './index.scss'

class AddPawn extends React.Component {
  static propTypes = {
    fen: PropTypes.object
  }
  generateCoords() {
    return [Math.floor(Math.random() * 7), Math.floor(Math.random() * 8)]
  }
  addNewPawn() {
    const coords = this.generateCoords()
    let matrixFEN = FENtoMatrix(this.props.fen.fen)
    if (matrixFEN[coords[0]][coords[1]] === 0) {
      matrixFEN[coords[0]][coords[1]] = 'P'
    } else {
      this.addNewPawn()
    }
    let FEN = toFEN(matrixFEN)
    this.props.dispatch(setFen(FEN))
  }
  render() {
    return <div className="game-pawn">
      <button className="game-pawn-button" onClick={() => this.addNewPawn()}>
        Add white pawn
      </button>
    </div>
  }
}

const mapStateToProps = state => {
  const { fen } = state
  return { fen }
};

export default connect(mapStateToProps)(AddPawn);
