import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';


import {FENtoMatrix} from '../../../scripts/FEN-parser.js';

import Frame from './Frame/Frame';
import Cells from './Cells/Cells';

import './index.scss'

class Board extends React.Component {
  static propTypes = {
    fen: PropTypes.object
  }
  static defaultProps ={
    fen: 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1'
  }

  render() {
    return <div className="board">
      <div className="board-wrapper">
        <Frame>
          <Cells
            cells = {FENtoMatrix(this.props.fen.fen)}
          />
        </Frame>
      </div>
    </div>
  }
}

const mapStateToProps = state => {
  const { fen } = state
  return { fen }
};

export default connect(mapStateToProps)(Board);
