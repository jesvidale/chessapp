import React from 'react';

import './index.scss';

class Frame extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      frame: {
        row: ['1','2','3','4','5','6','7','8'],
        col: ['H','G','F','E','D','C','B','A']
      },
    }
  }
  render() {
    return <div className="frame-board">
      <div className="frame-board-container">
        <div className="frame-board-rows-caption">
          { this.state.frame.row.map((item, key) => 
          <span className="frame-board-row-name" key={`row-${key}`}>{ item }</span>
          )}
        </div>
        <div className="frame-board-cols-caption">
          { this.state.frame.col.map((item, key) => 
          <span className="frame-board-col-name"  key={`col-${key}`}>{ item }</span>
          )}
        </div>
        <div className="frame-board-rows-caption frame-board-reverse">
          { this.state.frame.row.map((item, key) => 
          <span className="frame-board-row-name" key={`row-${key}`}>{ item }</span>
          )}
        </div>
        <div className="frame-board-cols-caption frame-board-reverse">
          { this.state.frame.col.map((item, key) => 
          <span className="frame-board-col-name"  key={`col-${key}`}>{ item }</span>
          )}
        </div>
        { this.props.children }
      </div>
    </div>
  }
}

export default Frame
