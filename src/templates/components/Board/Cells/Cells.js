import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { getTentative } from '../../../../scripts/moves.js';
import { toFEN } from '../../../../scripts/FEN-parser.js';

import { setFen } from '../../../../actions';

import './index.scss'

String.prototype.isLowerCase = function() {
  return this.valueOf().toLowerCase() === this.valueOf();
}

const initialState = {
  tentative: {
    coords: []
  },
  clickFrom: {
    piece: 0,
    coords: []
  }
}

class Cells extends React.Component {
  static propTypes = {
    cells: PropTypes.array.isRequired
  }
  constructor(props) {
    super(props);
    this.state = initialState
  }

  getPiece = (rowIndex, colIndex, piece) => {
    if (this.state.clickFrom.piece === 0) {
      this.selectPieceAndTentative(rowIndex, colIndex, piece)
    } else {
      this.movePiece(rowIndex, colIndex)
    }
  }

  selectPieceAndTentative(rowIndex, colIndex, piece) {
    if (piece !== 0) {
      // FORCE ONLY WHITES TO MOVE
      if (!piece.isLowerCase()) {
        const tentative = getTentative({rowIndex, colIndex}, piece, this.props.cells)
        this.setState({ tentative: { coords: getTentative({rowIndex, colIndex}, piece, this.props.cells) } })
        if (tentative.length) {
          this.setState({ clickFrom: { piece: piece, coords: [rowIndex, colIndex]} })
        }
      } else {
        this.setState(initialState)
      }
    } else {
      this.setState(initialState)
    }
  }

  movePiece(rowIndex, colIndex) {
    if (this.state.tentative.coords.length) {
      const toMove = this.state.tentative.coords.find(el => this.arrayEquals(el, [rowIndex, colIndex]))
      if (toMove !== undefined) {
        let newCells = [ ...this.props.cells]
        newCells[this.state.clickFrom.coords[0]][this.state.clickFrom.coords[1]] = 0
        newCells[toMove[0]][toMove[1]] = this.state.clickFrom.piece
        this.props.dispatch(setFen(toFEN(newCells)))
      }
      this.setState(initialState)
    }
  }
  arrayEquals(a, b) {
    return Array.isArray(a) && Array.isArray(b) && a.length === b.length && a.every((val, index) => val === b[index]);
  }
  render() {
    return <div className="cells">
      { this.props.cells.map((row, rowIndex) => 
      <div className="row" key={ row + rowIndex }>
        { row.map((col, colIndex) => 
        <div
          id={`c${rowIndex}${colIndex}`}
          className={`cell cell-${(colIndex + rowIndex)% 2 ? "dark":"light"}`}
          data-piece={col}
          data-selected={this.arrayEquals(this.state.clickFrom.coords, [rowIndex, colIndex]) ? true: false}
          data-tentative={this.state.tentative.coords.some(el => this.arrayEquals(el, [rowIndex, colIndex])) ? true: false}
          key={ col + colIndex }
          onClick={() => this.getPiece(rowIndex, colIndex, col)}
        ></div>
        )}
      </div>
      )}
    </div>
  }
}

const mapStateToProps = state => {
  const { fen } = state
  return { fen }
};

export default connect(mapStateToProps)(Cells);
