import React from 'react';

import Wrapper from '../../common/Wrapper'
import Board from '../../components/Board/Board'
import AddPawn from '../../components/AddPawn/AddPawn'
import './index.scss'

class Home extends React.Component {
  render() {
    return <div className="home-page">
      <Wrapper>
        <h1 className="page-title">
          <span className="page-title-string">Chess test</span>
          <span className="page-title-decorator">♜♞♝♛♚♟︎</span>
        </h1>
        <Board />
        <AddPawn />
      </Wrapper>
    </div>
  }
}

export default Home
